# Spring Batch: Job CSV Writer

In this example you will learn how to read some data from any database and export into a CSV file using Spring Batch architecture.