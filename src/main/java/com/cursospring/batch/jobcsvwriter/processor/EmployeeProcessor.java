package com.cursospring.batch.jobcsvwriter.processor;

import com.cursospring.batch.jobcsvwriter.dto.EmployeeDTO;
import com.cursospring.batch.jobcsvwriter.model.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EmployeeProcessor implements ItemProcessor<Employee, EmployeeDTO> {

    @Override
    public EmployeeDTO process(Employee employee) throws Exception {
        EmployeeDTO dto = new EmployeeDTO();
        dto.setEmployeeId(employee.getEmployeeId());
        dto.setFirstName(employee.getFirstName());
        dto.setLastName(employee.getLastName());
        dto.setEmail(employee.getEmail());
        dto.setAge(employee.getAge());
        log.info("Inside processor. Employee: {}", employee.toString());
        return dto;
    }
}