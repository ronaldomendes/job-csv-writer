package com.cursospring.batch.jobcsvwriter.job;

import com.cursospring.batch.jobcsvwriter.dto.EmployeeDTO;
import com.cursospring.batch.jobcsvwriter.mapper.EmployeeDBRowMapper;
import com.cursospring.batch.jobcsvwriter.model.Employee;
import com.cursospring.batch.jobcsvwriter.processor.EmployeeProcessor;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemStreamReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import javax.sql.DataSource;

import static com.cursospring.batch.jobcsvwriter.utils.Constants.AGE;
import static com.cursospring.batch.jobcsvwriter.utils.Constants.EMAIL;
import static com.cursospring.batch.jobcsvwriter.utils.Constants.EMPLOYEE_ID;
import static com.cursospring.batch.jobcsvwriter.utils.Constants.FIRSTNAME;
import static com.cursospring.batch.jobcsvwriter.utils.Constants.LASTNAME;
import static com.cursospring.batch.jobcsvwriter.utils.Constants.OUTPUT_FILE_PATH;
import static com.cursospring.batch.jobcsvwriter.utils.Constants.QUALIFIER_NAME;
import static com.cursospring.batch.jobcsvwriter.utils.Constants.STEP_NAME;

@Configuration
@AllArgsConstructor
public class DemoJob {

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final DataSource dataSource;
    private final EmployeeProcessor employeeProcessor;


    @Qualifier(value = QUALIFIER_NAME)
    @Bean
    public Job demoOneJob() throws Exception {
        return this.jobBuilderFactory.get(QUALIFIER_NAME).start(stepOneDemo()).build();
    }

    @Bean
    public Step stepOneDemo() throws Exception {
        return stepBuilderFactory.get(STEP_NAME)
                .<Employee, EmployeeDTO>chunk(10)
                .reader(employeeDBReader())
                .processor(employeeProcessor)
                .writer(employeeFileWriter())
                .build();
    }

    @Bean
    public ItemStreamReader<Employee> employeeDBReader() {
        JdbcCursorItemReader<Employee> reader = new JdbcCursorItemReader<>();
        reader.setDataSource(dataSource);
        reader.setSql("select * from tb_employee");
        reader.setRowMapper(new EmployeeDBRowMapper());
        return reader;
    }

    private ItemWriter<EmployeeDTO> employeeFileWriter() throws Exception {
        final Resource outputResource = new FileSystemResource(OUTPUT_FILE_PATH);
        FlatFileItemWriter<EmployeeDTO> writer = new FlatFileItemWriter<>();
        writer.setResource(outputResource);
        writer.setLineAggregator(defineLineAggregator());
        writer.setShouldDeleteIfExists(true);
        return writer;
    }

    private DelimitedLineAggregator<EmployeeDTO> defineLineAggregator() {
        DelimitedLineAggregator<EmployeeDTO> aggregator = new DelimitedLineAggregator<>();
        aggregator.setFieldExtractor(defineFieldExtractor());
        return aggregator;
    }

    private BeanWrapperFieldExtractor<EmployeeDTO> defineFieldExtractor() {
        BeanWrapperFieldExtractor<EmployeeDTO> extractor = new BeanWrapperFieldExtractor<>();
        extractor.setNames(new String[]{EMPLOYEE_ID, FIRSTNAME, LASTNAME, EMAIL, AGE});
        return extractor;
    }


}
