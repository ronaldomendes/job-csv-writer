package com.cursospring.batch.jobcsvwriter.utils;

public class Constants {
    public static final String CONTEXT_KEY_NAME = "fileName";
    public static final String FILE_NAME_CSV = "employees.csv";
    public static final String EMPLOYEE_ID = "employeeId";
    public static final String FIRSTNAME = "firstName";
    public static final String LASTNAME = "lastName";
    public static final String EMAIL = "email";
    public static final String AGE = "age";
    public static final String OUTPUT_FILE_PATH = "output/employee_output.csv";
    public static final String QUALIFIER_NAME = "demoOne";
    public static final String STEP_NAME = "stepOne";

    private Constants() {
    }
}
