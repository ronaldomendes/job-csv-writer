package com.cursospring.batch.jobcsvwriter;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBatchProcessing
@ComponentScan(basePackages = {"com.cursospring.batch.jobcsvwriter"})
public class JobCsvWriterApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobCsvWriterApplication.class, args);
    }

}
